import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {Provider} from "react-redux";
import EditUserInfo from './pages/editUserInfo'
import ListOfUsers from "./pages/listOfUsers";
import React, {Component} from "react";
import Store from "./strore/store";
import {getData} from "./strore/actions";

export default class App extends Component{
  componentDidMount() {
    Store.dispatch(getData())
  }
  render() {
    return (
        <div className="App">
          <Provider store={Store}>
            <Router>
                <Switch>
                  <Route exact path="/edit-user-info/:userId" component={EditUserInfo} />
                  <Route path="/">
                    <ListOfUsers />
                  </Route>
                </Switch>
            </Router>
          </Provider>
        </div>
    );
  }
}

