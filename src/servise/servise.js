

export default class GotService {
    constructor() {
        this._url = 'http://www.filltext.com/?rows=20&id={number|20}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&adress={addressObject}&description={lorem|32}'
    }

    async getResource() {
        const res  = await fetch(this._url,
            {
                headers: new Headers({
                    'Content-Type' : 'application/json',
                    'Accept-Charset' : 'utf-8',
                })
            })
        if(!res.ok) {
            throw new Error(`Could not fetch url: ${this._url} \n 
            Error: ${res.status}`)
        }
        const response = await res.json()
        return response
    }
}