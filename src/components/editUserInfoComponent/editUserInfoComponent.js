import React, {Component} from 'react'
import {connect} from "react-redux";
import { Button, Form, FormGroup, Label, Input, Container, Row, Col, Alert} from 'reactstrap';
import {editUserData} from '../../strore/actions'



class EditUserInfoComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            success: false
        }
    }

    componentDidMount() {
        this.setState({
            ...this.props.user
        })
    }

    onUpdateName = (event) => {
        this.setState({firstName: event.target.value})
    }

    onUpdateEmail = (event) => {
        this.setState({email: event.target.value})
    }

    onUpdatePhone = (event) => {
        this.setState({phone: event.target.value})
    }

    onUpdateCountry = (event) => {
        this.setState({country: event.target.value})
    }

    onUpdateAge = (event) => {
        this.setState({age: event.target.value})
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.setState({
            id: this.props.user.id,
            success: true
        })
       this.props.editUserData(this.props.user.id, this.props.state, this.state)

    }

    render() {
        const { id, firstName, lastName, phone, email, adress, description, success}  = this.props.user
        return (
            <Container>
                <Row>
                    <Col md={12}>
                        <h1 className="h1">{`${lastName} ${firstName}`}</h1>
                    </Col>
                </Row>
                <Form>
                    <FormGroup>
                        <Label for="exampleName">Name</Label>
                        <Input type="text" id="exampleName" onChange={this.onUpdateName} placeholder={firstName} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="examplePhone">Phone</Label>
                        <Input type="text" id="examplePhone" onChange={this.onUpdatePhone} placeholder={phone} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleEmail">Email</Label>
                        <Input type="email" id="exampleEmail" onChange={this.onUpdateEmail} placeholder={email} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="countryField">Country</Label>
                        <Input type="select" onChange={this.onUpdateCountry} id="countryField">
                            <option>Australia</option>
                            <option>Russia</option>
                            <option>USA</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleAge">Age</Label>
                        <Input type="text" id="exampleAge" onChange={this.onUpdateAge} placeholder={33} />
                    </FormGroup>
                    <Button onClick={this.onSubmit}>Submit</Button>
                </Form>
                { success? <Alert color="primary" className="mt-3">Data was saved successfully</Alert>: null}
                <Row>
                    <h2 className="text-left mt-3">Description:</h2>
                    <Col md={12}>
                        <p className="text-justify">{description}</p>
                    </Col>
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.data.find(elem => elem.id === Number(ownProps.userId.userId)),
        state: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        editUserData: (id, data, newItem) => dispatch(editUserData(id, data, newItem))
    }
}

export default connect(mapStateToProps, mapDispatchToProps )(EditUserInfoComponent)