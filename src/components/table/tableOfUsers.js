import React,{Component} from "react";
import { Table } from 'reactstrap';
import TableRow from "./tableRow";
import {connect} from "react-redux";
import FilterByNameButton from "./filterByNameButton/filterByNameButton";
import {filteredElementsByCol} from "../../strore/selectors";
import {deleteItem, filterByName} from "../../strore/actions";

class TableOfUsers extends Component {
    constructor(props) {
        super(props);
    }

    render() {
    const {data} = this.props.store
        const {onFilterByName, deleteItem} = this.props
        let visibleItem = filteredElementsByCol(this.props.store)
        return (
            <Table>
                <thead>
                <tr>
                    <th>UserId</th>
                    <FilterByNameButton onFilterByName={onFilterByName} />
                    <th>Phone</th>
                    <th>Email</th>
                    <th>City</th>
                    <th>Age</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                { visibleItem.map((item, i) => {

                    return (
                        <TableRow key={i} user={item} deleteItem={deleteItem} data={data}/>
                    )
                })}
                </tbody>
            </Table>
        )
    }
}

const mapStateToProps = (store) => {
    return {
        store: store
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFilterByName: (isFilter) => dispatch(filterByName(isFilter)),
        deleteItem: (id, data) => dispatch(deleteItem(id, data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TableOfUsers)