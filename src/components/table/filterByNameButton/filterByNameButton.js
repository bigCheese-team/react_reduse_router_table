import React, {useState} from 'react';

const FilterByNameButton = ({onFilterByName}) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    let [isFilter, setFilterState] = useState(false);
let btnStyle = "border border-primary"
    if(isFilter) {
        btnStyle = "alert alert-primary"
    }
    return (
        <th className={btnStyle} onClick={() => {
            setFilterState(isFilter = !isFilter)
            return onFilterByName(isFilter)
        }}>FirstName</th>
    )
}

export default  FilterByNameButton

