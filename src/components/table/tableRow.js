import React from "react";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";

const TableRow = ({user, deleteItem, data}) => {
    return (
        <tr>
            <th scope="row">{user.id}</th>
            <td>{user.firstName}</td>
            <td>{user.phone}</td>
            <td>{user.email}</td>
            <td>{user.adress.city}</td>
            <td>{Math.floor(Math.random() * 100)}</td>
            <td>
                <Link className="btn btn-info mr-2" to={`/edit-user-info/${user.id}`}>Edit</Link>
                <Button type="button" onClick={() => deleteItem(user.id, data)}>Delete</Button>
            </td>


        </tr>
    )
}

export default TableRow