import  {createSelector} from "reselect";

const allData = state => state.data
const  filterByName = state => state.filterByName



export const filteredElementsByCol  = createSelector(
    [allData, filterByName],
    (data, filter) => {
        let newData = []
        if(filter) {
            newData = data.sort((a,b) => {
                if(a.firstName < b.firstName) {
                    return 1
                }
                if(a.firstName > b.firstName ) {
                    return -1
                }
                return 0
            })
            return newData
        } else  {
            newData = data.sort((a,b) => {
                if(a.firstName > b.firstName) {
                    return 1
                }
                if(a.firstName < b.firstName ) {
                    return -1
                }
                return 0
            })
            return newData
        }
    }
)
