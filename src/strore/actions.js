import {
    GET_DATA_SUCCESS,
    GET_DATA_STARTED,
    GET_DATA_FAIL,
    SET_DATA_TO_STORE,
    FILTER_BY_NAME,
    DELETE_ITEM_FROM_DATA
} from './actionsTypes'
import GotService from "../servise/servise";

const gotService = new GotService()

export const getData = () => {

    return dispatch => {
        dispatch(getDataStarted())
        gotService.getResource()
            .then(res => {
                dispatch(getDataSuccess(res))
            })
            .catch(res => {
                dispatch(getDataFail(res.message))
            })
    }
}

const getDataStarted = () => ({
    type: GET_DATA_STARTED
})

const getDataSuccess = (data) => ({
    type: GET_DATA_SUCCESS,
    data: data,
    payload: data.length
})

const getDataFail = (error) => ({
    type: GET_DATA_FAIL,
    payload: error
})

export const editUserData = (id, data, newItem) => {

    const indexOfItem = data.data.findIndex(elem => elem.id === id)
    const newData = [...data.data.slice(0, indexOfItem), newItem, ...data.data.slice(indexOfItem + 1, data.data.length)]
    return {
        type: SET_DATA_TO_STORE,
        data: newData
    }
}

export const filterByName = (onFilter) => {
    return {
        type: FILTER_BY_NAME,
        payload: onFilter
    }
}

export const deleteItem = (id, data) => {
    const indexOfItem = data.findIndex(elem => elem.id === id)
    const newData = [...data.slice(0, indexOfItem),  ...data.slice(indexOfItem + 1, data.length)]
    return {
        type: DELETE_ITEM_FROM_DATA,
        data: newData
    }
}