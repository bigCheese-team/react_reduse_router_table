import {
    GET_DATA_SUCCESS,
    GET_DATA_STARTED,
    GET_DATA_FAIL,
    SET_DATA_TO_STORE,
    FILTER_BY_NAME,
    DELETE_ITEM_FROM_DATA
} from './actionsTypes'

let initialState = {
    data: [],
    loading: false,
    error: null,
    filterByName: false
}


const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_DATA_SUCCESS:
            return {
                data: action.data,
                loading: false,
                error: null
            }
        case GET_DATA_STARTED:
            return {
                ...state,
                loading: true
            }
        case GET_DATA_FAIL:
            return {
                ...state,
                error: action.payload
            }
        case SET_DATA_TO_STORE:
            return {
                ...state,
                data: action.data
            }
        case FILTER_BY_NAME:
            return {
                ...state,
                filterByName: action.payload
            }
        case DELETE_ITEM_FROM_DATA:
            return {
                ...state,
                data: action.data
            }

        default:
            return state
    }
}
export default reducer