import React from 'react'
import EditUserInfoComponent from "../components/editUserInfoComponent/editUserInfoComponent";


const EditUserInfo = ({ match, location }) => {
    const {userId}  = match.params
    return (
        <>
            <p>Edit info User with id:{userId}</p>
                <EditUserInfoComponent userId={{userId}} />
        </>
    )
}

export default EditUserInfo